import React, { useState } from 'react'
const Child = () => {
    const [data]=useState([
        {id:1,name:"aman"},
        {id:2,name:"pupu"},
        {id:3,name:"raj"},
        {id:4,name:"kumar"}
    ])
  return (
    <div>
        {
            data.map((item,index)=>{
                return <div key={index}>
                    <h5>{item.id}</h5>
                    <h5>{item.name}</h5>
                </div>
            })
        }
    </div>
  )
}

export default Child