import React from 'react'
import Child from './components/Child'
import First from './components/First'

const App = () => {
  return (
    <div>App
      <Child name="aman"/>
      <First/>
    </div>
  )
}

export default App